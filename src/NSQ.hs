{-# LANGUAGE DeriveGeneric   #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module NSQ where

import           Data.Aeson
import qualified Data.ByteString.Lazy as BSL
import           Data.List            as L
import           Data.String
import           Data.Text            as T
import           Data.Time
import           Network.HTTP.Conduit
import           Protolude
import           System.Environment   as SE

-- cassava
import           Data.Csv             (Header)
import qualified Data.Csv             as Cassava

-- vector
import qualified Data.Vector          as Vector

-- types
import           Types

topicNames :: [Text]
topicNames =
  [ "go_optimization_action"
  , "go_optimization_rule"
  , "go_optimization_rule_time_based_action"
  , "go_optimization_target"
  , "go_optimization_task"
  , "go_sync_ad"
  , "go_sync_ad_account"
  , "go_sync_ad_saver"
  , "go_sync_adset"
  , "go_sync_adset_saver"
  , "go_sync_campaign"
  , "go_sync_campaign_saver"
  , "go_sync_insight"
  , "go_sync_insight_breakdown"
  , "go_sync_insight_breakdown_saver"
  , "go_sync_insight_saver"
  , "go_sync_interest_data"
  , "go_sync_interest_data_saver"
  , "go_sync_interest_keyword_saver"
  , "go_sync_location_saver"
  , "go_sync_properties"
  , "optimization_rule"
  , "optimization_rule_time_based_action"
  , "sync_ad"
  , "sync_adset"
  , "sync_campaign"
  , "sync_insight"
  , "sync_insight_breakdown"
  ]

-- | Process NSQ `Topic`.
processNsq :: Text -> IO ()
processNsq topicName = do
  nsqStatisticURL <- SE.getEnv "NSQ_STAT_URL"
  let statisticURL topic channel =
        nsqStatisticURL
          ++ "?format=json&topic="
          ++ topic
          ++ "&channel="
          ++ channel
  jsonString <- lookupNsqStatistic $ statisticURL (unpack topicName) "job"
  let statistic = eitherDecode jsonString :: Either String Statistic
  case statistic of
    Left  err  -> putStrLn err
    Right stat -> processTopicStatistic stat topicName
  sendSlackNotification

-- | Lookup `Topic`'s `Statistic`.
lookupNsqStatistic :: String -> IO BSL.ByteString
lookupNsqStatistic url = simpleHttp url

-- | Process `Topic`'s `Statistic` and generate `Report` file.
processTopicStatistic :: Statistic -> Text -> IO ()
processTopicStatistic statistic topicName = do
  topicReports <- breakdownTopicReport (statisticTopics statistic) topicName
  reportToCSV $ L.foldl' (++) [] topicReports

-- | Map over `Topic` and return list of `Report`s.
breakdownTopicReport :: [Topic] -> Text -> IO [[Report]]
breakdownTopicReport topics topicName
  | topicName == "all" = do
    mapM (iterateTopicName topics) topicNames
  | otherwise = do
    mapM (iterateTopicName topics) [topicName]

-- | Populate `Report` for specified `Topic`.
iterateTopicName :: [Topic] -> Text -> IO [Report]
iterateTopicName topics topicName = do
  let topic    = filterTopicName topics topicName
      channels = case topic of
        Just t  -> topicChannels t :: [Channel]
        Nothing -> []
  reports <- populateReport topic channels
  return reports

-- | Get `Topic` from array of `Topic`.
filterTopicName :: [Topic] -> Text -> Maybe Topic
filterTopicName topics topicName =
  case L.filter (\x -> topicTopicName x == topicName) topics of
    t : _ -> Just t
    _     -> Nothing

-- | Map over the `Channel`s to get the `Report`s.
populateReport :: Maybe Topic -> [Channel] -> IO [Report]
populateReport Nothing      _     = return []
populateReport (Just topic) chans = mapM (populRep topic) chans
 where
  populRep t channel = do
    formattedDate <- getFormattedDate
    formattedTime <- getFormattedTime
    return $ Report (topicTopicName t)
                    (channelDepth channel)
                    (channelMessageCount channel)
                    (L.length $ channelClients channel)
                    formattedDate
                    formattedTime

-- | Get date which has been formatted into string.
getFormattedDate :: IO Text
getFormattedDate = do
  currentLocalTime <- getZonedTime
  return $ T.pack $ formatTime defaultTimeLocale "%Y-%m-%d" currentLocalTime

-- | Get time which has been formatted into string.
getFormattedTime :: IO Text
getFormattedTime = do
  currentLocalTime <- getZonedTime
  return $ T.pack $ formatTime defaultTimeLocale "%H:%M" currentLocalTime

-- | Put `Report`s into csv file.
reportToCSV :: [Report] -> IO ()
reportToCSV reports = do
  csvFile <- SE.getEnv "CSV_FILE"
  BSL.appendFile csvFile $ Cassava.encodeByName reportHeader reports

-- | List of header to be used in `Report`.
reportHeader :: Header
reportHeader = Vector.fromList
  ["Topic Name", "Message", "Total Message", "Connection", "Date", "Time"]

-- | Send notification to slack channel using web hook.
sendSlackNotification :: IO ()
sendSlackNotification = do
  manager <- newManager tlsManagerSettings
  let requestObject = object
        [ "name" .= ("NSQ Statistic Checker" :: String)
        , "text" .= ("NSQ Statistic Report has been compiled" :: String)
        ]

  slackURL       <- SE.getEnv "SLACK_URL"
  initialRequest <- parseRequest slackURL

  let request = initialRequest
        { method         = "POST"
        , requestHeaders = [("Content-Type", "application/json")]
        , requestBody    = RequestBodyLBS $ encode requestObject
        }
  response <- httpLbs request manager
  putStrLn $ responseBody response
