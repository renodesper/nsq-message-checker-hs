.PHONY: default build doc

VERSION := $(shell grep -o '[0-9].[0-9].[0-9].[0-9]' nsq-message-checker.cabal)
DOCROOT := $(shell stack path --local-doc-root)

default: build doc

build:
	stack build

doc:
	stack haddock
	rm -rf docs/haddock
	cp -r ${DOCROOT}/nsq-message-checker-${VERSION} docs/haddock

run:
	stack exec nsq-message-checker
