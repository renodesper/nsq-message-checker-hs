{-# LANGUAGE OverloadedStrings #-}

module Main where

import           NSQ
import           Protolude
import           Turtle

-- dotenv
import qualified Configuration.Dotenv       as Dotenv
import           Configuration.Dotenv.Types


-- main parser
parser :: Parser (IO ())
parser = parseMainCommand
     <|> parseProcessNsqTopic

-- main command
parseMainCommand :: Parser (IO ())
parseMainCommand = pure mainCommand

mainCommand :: IO ()
mainCommand = echo "Use \"-h\" argument for help"

-- topic subcommand
-- stack exec nsq-message-checker -- topic all
-- stack exec nsq-message-checker -- topic go_optimization_rule
parseProcessNsqTopic :: Parser (IO ())
parseProcessNsqTopic =
  fmap
    processNsq
    (subcommand
       "topic"
       "Process specified topic"
       (argText "topic_name" "What topic should be processed? (all for all topics)"))

-- main
main :: IO ()
main = do
  _ <- Dotenv.loadFile defaultConfig
  cmd <- options "NSQ Statistic Checker" parser
  cmd
